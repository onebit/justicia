﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
namespace AppBiz.Productos
{
    [DataContract]
    public sealed class Producto
    {
        
        [DataMember]
        public int Id { get; set; }

        
        [DataMember]
        public string ProductoNombre { get; set; }
        [DataMember]
        public decimal ProductoPrecio { get; set; }
        [DataMember]
        public decimal ProductoImpuesto { get; set; }
    }
}
