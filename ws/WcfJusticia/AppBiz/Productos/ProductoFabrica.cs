﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBiz.Productos
{
    public static class ProductoFabrica
    {
        public static Producto Construir(SqlDataReader reader)
        {
            var producto = new Producto();
            while (reader.Read())
            {
                var _productoNombre = reader.GetOrdinal("ProductoNombre");
                if (!reader.IsDBNull(_productoNombre))
                    producto.ProductoNombre = reader.GetString(_productoNombre);

                var _productoPrecio = reader.GetOrdinal("ProductoPrecio");
                if (!reader.IsDBNull(_productoPrecio))
                    producto.ProductoPrecio = reader.GetDecimal(_productoPrecio);


                var _productoImpuesto = reader.GetOrdinal("ProductoImpuesto");
                if (!reader.IsDBNull(_productoImpuesto))
                    producto.ProductoImpuesto = reader.GetDecimal(_productoImpuesto);
            }
            return producto;
        }
    }
}
