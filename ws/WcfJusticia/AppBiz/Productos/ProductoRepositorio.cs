﻿using AppBiz._Generica;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AppBiz.Productos
{
    public sealed class ProductoRepositorio : Repositorio
    {
        public void Crear(Producto producto)
        {
            var command = new SqlCommand("Producto_Create")
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("ProductoNombre", producto.ProductoNombre);
            command.Parameters.AddWithValue("ProductoPrecio", producto.ProductoPrecio);
            command.Parameters.AddWithValue("ProductoImpuesto", producto.ProductoImpuesto);
            MyExecuteNonQuery(command);

        }

        public void Actualizar(Producto producto)
        {
            SqlCommand command = new SqlCommand("Producto_Actualizar");
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("ProductoNombre", producto.ProductoNombre);
            command.Parameters.AddWithValue("ProductoPrecio", producto.ProductoPrecio);
            command.Parameters.AddWithValue("ProductoImpuesto", producto.ProductoImpuesto);
            MyExecuteNonQuery(command);

        }

        public bool Eliminar(Guid id)
        {
            return false;
        }

        public Producto Consultar(int id)
        {
            SqlCommand command = new SqlCommand("Producto_Consultar")
            {
                CommandType = CommandType.StoredProcedure
            };
            using (var reader = MyExecuteReader(command))
            {
                return ProductoFabrica.Construir(reader);
            }
        }

        public List<Producto> ConsultarPorPrecio(decimal precio)
        {
            var list = new List<Producto>();
            SqlCommand command = new SqlCommand("Producto_Consultar")
            {
                CommandType = CommandType.StoredProcedure
            };
            using (var reader = MyExecuteReader(command))
            {
                while (reader.Read())
                {
                    list.Add(ProductoFabrica.Construir(reader));
                }
            }
            return list;
        }

        public List<Producto> Consultar()
        {
            SqlCommand command = new SqlCommand("Producto_Consultar");
            command.CommandType = System.Data.CommandType.StoredProcedure;
            var reader = MyExecuteReader(command);

            var list = new List<Producto>();

            while (reader.Read())
            {
                var producto = new Producto();

                var _productoNombre = reader.GetOrdinal("ProductoNombre");
                if (!reader.IsDBNull(_productoNombre))
                    producto.ProductoNombre = reader.GetString(_productoNombre);

                var _productoPrecio = reader.GetOrdinal("ProductoPrecio");
                if (!reader.IsDBNull(_productoPrecio))
                    producto.ProductoPrecio = reader.GetDecimal(_productoPrecio);


                var _productoImpuesto = reader.GetOrdinal("ProductoImpuesto");
                if (!reader.IsDBNull(_productoImpuesto))
                    producto.ProductoImpuesto = reader.GetDecimal(_productoImpuesto);

                list.Add(producto);
            }

            return list;
        }



        public DataTable ConsultarDataTable()
        {
            SqlCommand command = new SqlCommand("Producto_Consultar");
            command.CommandType = System.Data.CommandType.StoredProcedure;

            return MyExecuteDataSet(command);
        }
    }

}
