﻿using AppBiz.Clientes;
using AppBiz.Productos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBiz
{
    public class Tienda
    {
        public ProductoRepositorio Producto => new ProductoRepositorio();
        public ClienteRepositorio Cliente => new ClienteRepositorio();

    }
}
