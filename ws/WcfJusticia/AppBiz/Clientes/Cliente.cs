﻿namespace AppBiz
{
    public enum TipoCliente
    {
        Natural,
        Juridico
    }
    /// <summary>
    /// Clase tipo entidad (entity) CLIENTE
    /// </summary>
    public sealed class Cliente
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        private string Identificacion { get; set; }
        private string Direccion { get; set; }
        private string Telefono { get; set; }
        public string Email { get; set; }
        //public TipoCliente TipoCliente { get; set; }
    }
}
