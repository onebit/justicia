﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBiz.Polimorfismo
{
    class Figura
    {
        protected double _b { get; set; }
        protected double _h { get; set; }

        public virtual double area()
        {
            return 0;
        }
    }

    class FiguraRectangulo : Figura
    {
        public override double area()
        {
            return (_b * _h);
        }
    }
    class FiguraTriangulo : Figura
    {
        public double area()
        {
            return (_b * _h) / 2.0f;
        }
    }



  
}
