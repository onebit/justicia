﻿using AppBiz.Facturas;
using System.Collections.Generic;

namespace AppBiz.Facturas
{
    public sealed class Factura
    {
        public Factura()
        {
            Detalle = new List<FacturaDetalle>();
        }
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public string Consecutivo { get; set; }
        public List<FacturaDetalle> Detalle { get; set; }
    }
}
