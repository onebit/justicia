﻿using AppBiz._Generica;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AppBiz.Facturas
{
    public sealed class FacturaRepositorio : Repositorio
    {
        public void Crear(Factura factura1)
        {
            using (var conexion = new SqlConnection(ConnectionString))
            {
                conexion.Open();
                var transaccion = conexion.BeginTransaction();
                try
                {
                    var command = new SqlCommand("Factura_Create", conexion, transaccion)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    command.Parameters.AddWithValue("Consecutivo", factura1.Consecutivo);
                    command.Parameters.AddWithValue("IdCliente", factura1.IdCliente);
                    var idFactura = command.ExecuteScalar();
                    /* INSERTO EL DETALLE DE LA FACTURA */
                    var commandDetalle = new SqlCommand("FacturaDetalle_Create", conexion, transaccion)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    
                    foreach (var detalle in factura1.Detalle)
                    {
                        commandDetalle.Parameters.AddWithValue("FacturaId", idFactura);
                        commandDetalle.Parameters.AddWithValue("Cantidad", detalle.Cantidad);
                        commandDetalle.Parameters.AddWithValue("Precio", detalle.Precio);
                        commandDetalle.Parameters.AddWithValue("ProductoId", detalle.ProductoId);
                        commandDetalle.Parameters.AddWithValue("ValorIva", detalle.ValorIva);
                        commandDetalle.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                    transaccion.Commit();
                }
                catch (Exception)
                {
                    transaccion.Rollback();
                    throw;
                }
            }
        }
    }
}
