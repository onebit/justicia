﻿using AppBiz.Productos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfJusticia.SrvProductos
{
    [ServiceContract]
    public interface IProductos
    {
        [OperationContract]
        bool CrearProducto(Producto producto);

        [OperationContract]
        List<Producto> GetProductos();

        [OperationContract]
        List<Producto> GetProductosPorRangoPrecios(string rangoMenor, string rangoMayor);
    }
}
