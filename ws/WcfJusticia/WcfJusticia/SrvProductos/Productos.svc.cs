﻿using AppBiz;
using AppBiz.Productos;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace WcfJusticia.SrvProductos
{
    public class Productos : IProductos
    {
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "crearProductos")]
        public bool CrearProducto(Producto producto)
        {
            Tienda tienda = new Tienda();

            tienda.Producto.Crear(producto);
            return true;
        }

        [WebInvoke(Method ="GET",
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate ="productos")]
        public List<Producto> GetProductos()
        {
            Tienda tienda = new Tienda();

            return tienda.Producto.Consultar();
        }



        [WebInvoke(Method = "GET",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "findProductosPorRango/{rangoMenor}/{rangoMayor}")]
        public List<Producto> GetProductosPorRangoPrecios(string rangoMenor, string rangoMayor)
        {
            Tienda tienda = new Tienda();

            return tienda.Producto.Consultar().FindAll(p => p.ProductoPrecio >= decimal.Parse(rangoMenor) && p.ProductoPrecio <= decimal.Parse(rangoMayor));
        }
    }
}
