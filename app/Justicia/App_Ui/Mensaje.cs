﻿namespace Justicia.App_Code
{
    public static class Mensaje
    {
        public static string Error(string message) {
            return @"<div class=""alert alert-danger"" role=""alert"">" + message + "</div>";
        }

        public static string Success(string message)
        {
            return @"<div class=""alert alert-success"" role=""alert"">" + message + "</div>";
        }

        public static string Info(string message)
        {
            return @"<div class=""alert alert-info"" role=""alert"">" + message + "</div>";
        }
    }
}