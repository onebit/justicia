﻿using Justicia.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Justicia
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_enviar_Click(object sender, EventArgs e)
        {
            Calcular();
        }

        private void Calcular()
        {
            try
            {
                var resultado = "";

                if (Ddl_Operacion.SelectedValue == "s")
                {
                    resultado = (decimal.Parse(txb_alto.Text) + Convert.ToDecimal(txb_lado.Text)).ToString();
                }
                else if (Ddl_Operacion.SelectedValue == "m")
                {
                    resultado = (decimal.Parse(txb_alto.Text) * Convert.ToDecimal(txb_lado.Text)).ToString();
                }
                else if (Ddl_Operacion.SelectedValue == "d")
                {
                    resultado = (decimal.Parse(txb_alto.Text) / Convert.ToDecimal(txb_lado.Text)).ToString();
                }
                else if (Ddl_Operacion.SelectedValue == "r")
                {
                    resultado = (decimal.Parse(txb_alto.Text) - Convert.ToDecimal(txb_lado.Text)).ToString();
                }
                else
                {
                    resultado = "No hay";
                }

                ltr_resultado.Text = resultado;


                for (int i = 0; i < Ddl_Operacion.Items.Count; i++)
                {

                    ltr_resultado.Text += Ddl_Operacion.Items[i].Text;
                }

                foreach (ListItem item in Ddl_Operacion.Items)
                {
                    ltr_resultado.Text += item.Text;
                }

            }
            catch (FormatException)
            {
                Ltr_Message.Text = Mensaje.Error("Ingrese solo numeros");
            }
            catch (Exception ex)
            {
                Ltr_Message.Text = Mensaje.Error(ex.Message);
            }
        }

        protected void ArmarMatrix_Click(object sender, EventArgs e)
        {
            int[,] array2D = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };
            BuildMatrix(array2D);
        }

        private void BuildMatrix(int[,] array2D)
        {
            try
            {
                //int i = int.Parse(Txb_I.Text);
                //int j = int.Parse(Txb_J.Text);

                //StringBuilder sb = new StringBuilder();
                //sb.AppendLine("<table>");
                //for (var x = 0; x < i; x++)
                //{
                //    sb.AppendLine("<tr>");
                //    for (var y = 0; y < j; y++)
                //    {
                //        sb.AppendLine("<td>"+ i+","+j+"</td>");
                //    }
                //    sb.AppendLine("</tr>");
                //}
                //sb.AppendLine("</table>");

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<table>");
                for (var x = 0; x < array2D.Length; x++)
                {
                    for (var y = 0; y < array2D.Length; y++)
                    {
                        var arr = array2D[x, y];
                    }
                }
                sb.AppendLine("</table>");
            }
            catch (Exception ex)
            {
                Ltr_Message.Text = Mensaje.Error(ex.Message);
            }

        }

        private void Arreglos()
        {

            string[,] customers = new string[4, 4];

            // Two-dimensional array.
            int[,] array2D = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };

            // The same array with dimensions specified.
            int[,] array2Da = new int[4, 2] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };
            // A similar array with string elements.
            string[,] array2Db = new string[3, 2] { { "one", "two" }, { "three", "four" },
                                        { "five", "six" } };

            // Three-dimensional array.
            int[,,] array3D = new int[,,] { { { 1, 2, 3 }, { 4, 5, 6 } },
                                 { { 7, 8, 9 }, { 10, 11, 12 } } };
            // The same array with dimensions specified.
            int[,,] array3Da = new int[2, 2, 3] { { { 1, 2, 3 }, { 4, 5, 6 } },
                                       { { 7, 8, 9 }, { 10, 11, 12 } } };
        }


    }
}