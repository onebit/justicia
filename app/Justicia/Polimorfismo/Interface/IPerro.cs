﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBiz.Polimorfismo.Interface
{
    interface IPerro
    {
        string Ladrar();
        string Dormir();

    }
}
