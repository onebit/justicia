﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBiz.Polimorfismo.Interface
{
    class Chihuahua : IPerro
    {
        public string Dormir()
        {
            return "Chihuahua durmiendo";
        }

        public string Ladrar()
        {
            return "Chihuahua ladrando";
        }
    }
}
