﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBiz.Polimorfismo.Abstraccion
{
    public abstract class Perro
    {
        public virtual string Ladrar() {
            return "Perro ladrando";
        }

        public abstract string Dormir();
    }

    public class Chihuahua : Perro
    {
        public override string Dormir()
        {
            return "Chihuahua durmiendo";
        }
    }
}
