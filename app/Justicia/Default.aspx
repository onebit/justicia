﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Justicia._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Literal ID="Ltr_Message" runat="server"></asp:Literal>
    <asp:DropDownList ID="Ddl_Operacion" runat="server">
        <asp:ListItem Value="s">Sumar</asp:ListItem>
        <asp:ListItem Value="m">Multiplicar</asp:ListItem>
        <asp:ListItem Value="d">Dividir</asp:ListItem>
        <asp:ListItem Value="r">Retar</asp:ListItem>
    </asp:DropDownList>
    <asp:TextBox ID="txb_lado" runat="server"></asp:TextBox>
    <asp:TextBox ID="txb_alto" runat="server"></asp:TextBox>
    <asp:Button ID="btn_enviar" runat="server" Text="Enviar" OnClick="btn_enviar_Click" />
    <asp:Literal ID="ltr_resultado" runat="server"></asp:Literal>

    <div class="well">
        <asp:TextBox runat="server" ID="Txb_I"></asp:TextBox>
        <asp:TextBox runat="server" ID="Txb_J"></asp:TextBox>
        <asp:Button runat="server" ID="ArmarMatrix" OnClick="ArmarMatrix_Click" Text="Armar" />
        <asp:Literal runat="server" ID="Ltr_Matrix"></asp:Literal>
    </div>

</asp:Content>
