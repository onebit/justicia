﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Justicia.Productos.Default" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Literal ID="Ltr_Mensaje" runat="server"></asp:Literal>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:LinkButton runat="server" CssClass="btn btn-info" ID="Lnb_Exportar" OnClick="Lnb_Exportar_Click">
        Exportar <i class="fas fa-file-excel"></i>
    </asp:LinkButton>

    <hr />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-sm-6">
                    <asp:GridView CssClass="table table-bordered table-hover table-striped" ID="Grv_Productos" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:BoundField HeaderStyle-HorizontalAlign="Center" DataField="ProductoNombre" HeaderText="Producto" />
                            <asp:BoundField HeaderStyle-HorizontalAlign="Center" DataField="ProductoNombre" HeaderText="Precio" />
                            <asp:TemplateField HeaderText="Impuesto">
                                <ItemTemplate>
                                    <%# Eval("ProductoImpuesto","{0:p}") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="col-sm-6">
                   
                </div>
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>

     <asp:Chart ID="Chr_Precios" runat="server" DataSourceID="SqlDataSource1">
                        <Series>
                            <asp:Series Name="Series1" XValueMember="ProductoNombre" YValueMembers="ProductoPrecio"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:tiendaConnectionString %>" SelectCommand="SELECT [Id], [ProductoNombre], [ProductoPrecio], [ProductoImpuesto] FROM [Producto]"></asp:SqlDataSource>
    <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource1">
        <Series>
            <asp:Series ChartType="Bar" Name="Series1" XValueMember="ProductoNombre" YValueMembers="ProductoPrecio">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</asp:Content>
