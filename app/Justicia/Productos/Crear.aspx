﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Crear.aspx.cs" Inherits="Justicia.Productos.Crear" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col col-6">
                    <asp:Literal ID="Ltr_Mensaje" runat="server"></asp:Literal>
                    <div class="form-group">
                        <label>Nombre</label>
                        <asp:TextBox ID="Txb_ProductoNombre" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="Txb_ProductoNombre" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Requerido"></asp:RequiredFieldValidator>

                    </div>

                    <div class="form-group">
                        <label>Precio</label>
                        <asp:TextBox TextMode="Number" ID="Txb_ProductoPrecio" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>


                    <div class="form-group">
                        <label>Impuesto</label>
                        <asp:TextBox TextMode="Number" ID="Txb_ProductoImpuesto" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <asp:Button OnClientClick="bloqueo" ID="Btn_Guardar" runat="server" CssClass="btn btn-primary" Text="Guardar" OnClick="Btn_Guardar_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
