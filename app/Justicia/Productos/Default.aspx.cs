﻿using AppBiz;
using AppBiz.Productos;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml.Style;
using Justicia.App_Code;
using System.Web.UI.DataVisualization.Charting;

namespace Justicia.Productos
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarGrilla();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarGrilla()
        {
            try
            {
                ProductoRepositorio productoRepositorio = new ProductoRepositorio();

                //var dt = productoRepositorio.ConsultarDataTable();

                //var dr = dt.Rows[1];
                //var dc = dr[1];
                //var c = dc;



                //Ordenar por nombre
                //lista.Sort(delegate (Producto x, Producto y)
                //{
                //    return x.ProductoNombre.CompareTo(y.ProductoNombre);
                //});

                //Filtro los productos que tengan precio mayor a 1000
                //Grv_Productos.DataSource = lista.FindAll(p => p.ProductoPrecio > 1000);


                //---ASI SE CARGA DESDE UN WEB SERVICE

                //var clienteProductos = new ServiceProductos.ProductosClient();
                //Grv_Productos.DataSource = clienteProductos.GetProductos();

                var lista = productoRepositorio.Consultar();
                Grv_Productos.DataSource = lista;
                Grv_Productos.DataBind();

                //Chr_Precios.Titles.Add("Precios");
                //Chr_Precios.Palette = ChartColorPalette.EarthTones;
                //foreach (var producto in  lista)
                //{
                //    Series series = Chr_Precios.Series.Add(producto.ProductoNombre);
                   
                //    series.Points.Add((double)producto.ProductoPrecio);
                //}

                ////Chr_Precios.DataSource
                //Chr_Precios.DataBind();
            }
            catch (Exception ex)
            {
                Ltr_Mensaje.Text = ex.Message;
            }
        }

        protected void Lnb_Exportar_Click(object sender, EventArgs e)
        {
            //ExportarAExcel();
            ExportarAExcelGuardar();
        }

        /// <summary>
        /// Genera un excel en memoria y lo lanza al usuario
        /// </summary>
        private void ExportarAExcel()
        {
            try
            {
                //var nombreArchivo = Server.MapPath("App_File/") + "productos.xlsx";
                var productos = Tienda.Producto.Consultar();
                var package = new ExcelPackage();
                var ws = CreateSheet(package, "Productos", 1);

                //Header
                ws.Cells[1, 1].Value = "Id";
                ws.Cells[1, 2].Value = "Producto";
                ws.Cells[1, 3].Value = "Precio";
                ws.Cells[1, 4].Value = "Impuesto";

                ws.Cells[1, 1].Style.Font.Bold = true;
                ws.Cells[1, 2].Style.Font.Bold = true;
                ws.Cells[1, 3].Style.Font.Bold = true;
                ws.Cells[1, 4].Style.Font.Bold = true;


                //Body
                var n = 2;
                foreach (var producto in productos)
                {
                    ws.Cells[n, 1].Value = producto.Id;
                    ws.Cells[n, 2].Value = producto.ProductoNombre;
                    ws.Cells[n, 3].Value = producto.ProductoPrecio;
                    ws.Cells[n, 4].Value = producto.ProductoImpuesto;
                    n++;
                }              

                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment;  filename=Productos.xlsx");
                Response.Clear();
                package.SaveAs(Response.OutputStream);
                Response.End();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Genera un archivo de excel y lo guarda en la carpeta especificada
        /// </summary>
        private void ExportarAExcelGuardar()
        {
            try
            {
                
                var nombreArchivo = Server.MapPath("~/App_File/") + "productos.xlsx";

                FileInfo newFile = new FileInfo(nombreArchivo);

                var productos = Tienda.Producto.Consultar();
                var package = new ExcelPackage();
                var ws = CreateSheet(package, "Productos", 1);

                //Header
                ws.Cells[1, 1].Value = "Id";
                ws.Cells[1, 2].Value = "Producto";
                ws.Cells[1, 3].Value = "Precio";
                ws.Cells[1, 4].Value = "Impuesto";

                ws.Cells[1, 1].Style.Font.Bold = true;
                ws.Cells[1, 2].Style.Font.Bold = true;
                ws.Cells[1, 3].Style.Font.Bold = true;
                ws.Cells[1, 4].Style.Font.Bold = true;


                //Body
                var n = 2;
                foreach (var producto in productos)
                {
                    ws.Cells[n, 1].Value = producto.Id;
                    ws.Cells[n, 2].Value = producto.ProductoNombre;
                    ws.Cells[n, 3].Value = producto.ProductoPrecio;
                    ws.Cells[n, 4].Value = producto.ProductoImpuesto;
                    n++;
                }
                //package.Save();
                package.SaveAs(newFile);
                //Response.ContentType = "application/vnd.ms-excel";
                //Response.AddHeader("content-disposition", "attachment;  filename=Productos.xlsx");
                //Response.Clear();
                //package.SaveAs(Response.OutputStream);
                //Response.End();
            }
            catch (Exception ex)
            {
                Ltr_Mensaje.Text = Mensaje.Error(ex.Message);
            }
        }

        public static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName, int sheetId)
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[sheetId];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet
            return ws;
        }
    }
}