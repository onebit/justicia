﻿using Justicia.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppBiz.Productos;
using System.Threading;
using AppBiz;

namespace Justicia.Productos
{
    public partial class Crear : System.Web.UI.Page
    {
        //Tienda tienda = new Tienda();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_Guardar_Click(object sender, EventArgs e)
        {
            Agregar();
        }

        private void Agregar()
        {
            try
            {
                Btn_Guardar.Enabled = false;
                Thread.Sleep(2000);
                

                var producto = new ServiceProductos.Producto()
                {
                    ProductoNombre = Txb_ProductoNombre.Text,
                    ProductoPrecio = decimal.Parse(Txb_ProductoPrecio.Text),
                    ProductoImpuesto = decimal.Parse(Txb_ProductoImpuesto.Text)
                };
                var clienteProductos = new ServiceProductos.ProductosClient();
                //Tienda.Producto.Crear(producto);
                clienteProductos.CrearProducto(producto);
                Ltr_Mensaje.Text = Mensaje.Success("Registro creado con exito!");
                Btn_Guardar.Enabled = true;

                
            }
            catch (Exception ex)
            {
                Ltr_Mensaje.Text = Mensaje.Error(ex.Message);
            }
        }
    }
}