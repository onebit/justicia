﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Justicia
{
    /// <summary>
    /// Ejemplo de colecciones
    /// </summary>
    public partial class Colecciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            List<string> lista = new List<string>
            {
                "fdfd",
                "fdfd",
                "fdfd",
                "fdfd",
                "fdfd"
            };

            var cliente1 = new Customer
            {
                Nombres = "Carlos",
                Apellidos = "Angulo"
            };
            var cliente2 = new Customer
            {
                Nombres = "Pepe",
                Apellidos = "Cortisona"
            };

            List<Customer> clientes = new List<Customer>
            {
                cliente1,
                cliente2
            };

            var resultado = clientes.FindAll(c => c.Apellidos.StartsWith("Angulo"));
            if (resultado != null) {

            }

            Dictionary<int, string> dic = new Dictionary<int, string>();

            dic.Add(2, "Yayita");
            dic.Add(3, "Gargant");
            dic.Add(2, "Mario Bros");

            Hashtable table = new Hashtable
            {
                { 324, 34 }
            };

            ArrayList arrayList = new ArrayList();
            arrayList.Add("Luigi");

        }
    }

    public class Customer
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
    }
}