﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace AppBiz._Generica
{
    public abstract class Repositorio
    {
        public string ConnectionString { get; private set; }

        public Repositorio()
        {
            //Traigo la conexion definida en el web.config
            ConnectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
            //var x = ConfigurationManager.AppSettings["x"];
        }

        public int MyExecuteNonQuery(SqlCommand command)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                command.Connection = connection;
                return command.ExecuteNonQuery();
            }
        }

        public object MyExecuteScalar(SqlCommand command)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                command.Connection = connection;
                return command.ExecuteScalar();
            }
        }

        public SqlDataReader MyExecuteReader(SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            command.CommandType = CommandType.StoredProcedure;
            connection.Open();
            command.Connection = connection;
            return command.ExecuteReader(CommandBehavior.CloseConnection);

        }

        public DataTable MyExecuteDataSet(SqlCommand command)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                command.Connection = connection;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                sqlDataAdapter.Fill(ds);
                /* ------- */
                //ds.AcceptChanges
                /* ------ */
                return ds.Tables[0];
            }
        }

        public Task<object> MyExecuteScalarAsync(SqlCommand command)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                command.Connection = connection;
                return command.ExecuteScalarAsync();
            }
        }
    }
}
