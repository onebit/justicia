﻿using AppBiz.Clientes;
using AppBiz.Productos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBiz
{
    public static class Tienda
    {
        public static ProductoRepositorio Producto => new ProductoRepositorio();
        public static ClienteRepositorio Cliente => new ClienteRepositorio();

    }
}
