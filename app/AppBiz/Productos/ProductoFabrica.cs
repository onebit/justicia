﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AppBiz.Productos
{
    public static class ProductoFabrica
    {
        private static Producto Construir(IDataRecord reader)
        {
            var producto = new Producto();

            var _id = reader.GetOrdinal("Id");
            if (!reader.IsDBNull(_id))
                producto.Id = reader.GetInt32(_id);

            var _productoNombre = reader.GetOrdinal("ProductoNombre");
            if (!reader.IsDBNull(_productoNombre))
                producto.ProductoNombre = reader.GetString(_productoNombre);

            var _productoPrecio = reader.GetOrdinal("ProductoPrecio");
            if (!reader.IsDBNull(_productoPrecio))
                producto.ProductoPrecio = reader.GetDecimal(_productoPrecio);


            var _productoImpuesto = reader.GetOrdinal("ProductoImpuesto");
            if (!reader.IsDBNull(_productoImpuesto))
                producto.ProductoImpuesto = reader.GetDecimal(_productoImpuesto);

            return producto;
        }

        public static List<Producto> ConstruirVarios(SqlDataReader reader)
        {
            try
            {
                var list = new List<Producto>();
                while (reader.Read())
                {
                    list.Add(Construir(reader));
                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }

        public static Producto ConstruirUno(SqlDataReader reader)
        {
            try
            {
                if (reader.Read())
                {
                    return Construir(reader);
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
    }
}
