﻿using AppBiz._Generica;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AppBiz.Productos
{
    public sealed class ProductoRepositorio : Repositorio
    {
        public void Crear(Producto producto)
        {
            var command = new SqlCommand("Producto_Create")
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("ProductoNombre", producto.ProductoNombre);
            command.Parameters.AddWithValue("ProductoPrecio", producto.ProductoPrecio);
            command.Parameters.AddWithValue("ProductoImpuesto", producto.ProductoImpuesto);
            MyExecuteNonQuery(command);

        }

        public void Actualizar(Producto producto)
        {
            SqlCommand command = new SqlCommand("Producto_Actualizar")
            {
                CommandType = CommandType.StoredProcedure
            };

            command.Parameters.AddWithValue("ProductoNombre", producto.ProductoNombre);
            command.Parameters.AddWithValue("ProductoPrecio", producto.ProductoPrecio);
            command.Parameters.AddWithValue("ProductoImpuesto", producto.ProductoImpuesto);
            MyExecuteNonQuery(command);
        }

        public bool Eliminar(Guid id)
        {
            return false;
        }

        public Producto Consultar(int id)
        {
            SqlCommand command = new SqlCommand("Producto_Consultar");
            using (var reader = MyExecuteReader(command))
            {
                return ProductoFabrica.ConstruirUno(reader);
            }
        }

        public List<Producto> ConsultarPorPrecio(decimal precio)
        {
            SqlCommand command = new SqlCommand("Producto_Consultar");
            using (var reader = MyExecuteReader(command))
            {
                return ProductoFabrica.ConstruirVarios(reader);
            }
        }

        public List<Producto> Consultar()
        {
            SqlCommand command = new SqlCommand("Producto_Consultar");
            using (var reader = MyExecuteReader(command))
            {
                return ProductoFabrica.ConstruirVarios(reader);
            }
        }



        public DataTable ConsultarDataTable()
        {
            SqlCommand command = new SqlCommand("Producto_Consultar");
            command.CommandType = CommandType.StoredProcedure;

            return MyExecuteDataSet(command);
        }
    }

}
