﻿namespace AppBiz.Productos
{
    public sealed class Producto
    {
        public int Id { get; set; }
        public string ProductoNombre { get; set; }
        public decimal ProductoPrecio { get; set; }
        public decimal ProductoImpuesto { get; set; }
    }
}
